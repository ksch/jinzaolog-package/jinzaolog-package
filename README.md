#common_logs
1、在config/app.php文件中providers数组里加入：
  Jinzaolog\Provider\LaravelSLSServiceProvider::class
  
2、在config/app.php文件中的aliases数组里加入
  'JinzaoLog' => Jinzaolog\Facade\WriterFacade::class,
  'AliLog' => Jinzaolog\Facade\AliLogFacade::class,

3、生成配置文件
    php artisan vendor:publish --provider="Jinzaolog\Provider\JinzaoLogServiceProvider"
    php artisan vendor:publish --provider="Jinzaolog\Provider\LaravelSLSServiceProvider" 
4、生成ide_helper
    php artisan ide-helper:generate
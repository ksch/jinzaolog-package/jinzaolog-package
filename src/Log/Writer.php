<?php

namespace Jinzaolog\Log;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Logging\Log as LogContract;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Psr\Log\LoggerInterface as PsrLoggerInterface;

class Writer implements LogContract, PsrLoggerInterface
{

    /**
     * @var Dispatcher
     */
    protected $dispatcher;

    /**
     * @var SLSLog
     */
    private $logger;

    /**
     * @var string
     */
    private $topic;

    protected $context = [];


    public function __construct(SLSLog $logger, Dispatcher $dispatcher = null, $topic)
    {
        if (isset( $dispatcher )) {
            $this->dispatcher = $dispatcher;
        }
        $this->logger = $logger;
        $this->topic  = $topic;
    }


    /**
     * Log an alert message to the logs.
     *
     * @param  string $topic
     * @param  array  $context
     *
     * @return void
     */
    public function alert($topic = '', array $context = [ ])
    {
        $this->writeLog(__FUNCTION__, $topic, $context);
    }


    /**
     * Log a critical message to the logs.
     *
     * @param  string $topic
     * @param  array  $context
     *
     * @return void
     */
    public function critical($topic = '', array $context = [ ])
    {
        $this->writeLog(__FUNCTION__, $topic, $context);
    }


    /**
     * Log an error message to the logs.
     *
     * @param  string $topic
     * @param  array  $context
     *
     * @return void
     */
    public function error($topic = '', array $context = [ ])
    {
        $this->writeLog(__FUNCTION__, $topic, $context);
    }


    /**
     * Log a warning message to the logs.
     *
     * @param  string $topic
     * @param  array  $context
     *
     * @return void
     */
    public function warning($topic = '', array $context = [ ])
    {
        $this->writeLog(__FUNCTION__, $topic, $context);
    }


    /**
     * Log a notice to the logs.
     *
     * @param  string $topic
     * @param  array  $context
     *
     * @return void
     */
    public function notice($topic = '', array $context = [ ])
    {
        $this->writeLog(__FUNCTION__, $topic, $context);
    }


    /**
     * Log an informational message to the logs.
     *
     * @param  string $topic
     * @param  array  $context
     *
     * @return void
     */
    public function info($topic = '', array $context = [ ])
    {
        $this->writeLog(__FUNCTION__, $topic, $context);
    }


    /**
     * Log a debug message to the logs.
     *
     * @param  string $topic
     * @param  array  $context
     *
     * @return void
     */
    public function debug($topic = '', array $context = [ ])
    {
        $this->writeLog(__FUNCTION__, $topic, $context);
    }


    /**
     * Log a message to the logs.
     *
     * @param  string $level
     * @param  string $topic
     * @param  array
     *$context
     * @return void
     */
    public function log($level, $topic = '', array $context = [ ])
    {
        $this->writeLog($level, $topic, $context);
    }

    /**
     * Register a file log handler.
     *
     * @param  string $path
     * @param  string $level
     *
     * @return void
     */
    public function useFiles($path, $level = 'debug')
    {

    }


    /**
     * Register a daily file log handler.
     *
     * @param  string $path
     * @param  int    $days
     * @param  string $level
     *
     * @return void
     */
    public function useDailyFiles($path, $days = 0, $level = 'debug')
    {

    }

    /**
     * System is unusable.
     *
     * @param string $topic
     * @param array  $context
     *
     * @return null
     */
    public function emergency($topic = '', array $context = array())
    {
        $this->writeLog(__FUNCTION__, $topic, $context);
    }

    /**
     * 写入日志内容
     * @param $column
     * @param null $value
     * @return $this
     */
    public function setData($column, $value = null) {
        if (is_array($column)) {
            foreach ($column as $key => $item) {
                if($key && $item){
                    $this->context[$key] = $item;
                }
            }
        } else if($column && $value){
            $this->context[$column] = $value;
        }
        return $this;
    }


    /**
     * Write a message to Monolog.
     *
     * @param  string $level
     * @param  string $topic
     * @param  array  $context
     *
     */
    protected function writeLog($level, $topic = '', $context)
    {
        $data = array_merge($this->context, $context);
        $data['level'] = $level;

        $this->logger->putLogs($data, $topic ? : $this->topic);
    }

    /**
     * Format the parameters for the logger.
     *
     * @param  mixed $message
     *
     * @return mixed
     */
    protected function formatMessage($message)
    {
        if (is_array($message)) {
            return var_export($message, true);
        } elseif ($message instanceof Jsonable) {
            return $message->toJson();
        } elseif ($message instanceof Arrayable) {
            return var_export($message->toArray(), true);
        }

        return $message;
    }

    /**
     * @param $topic
     * @return $this
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
        return $this;
    }

    /**
     * @return string
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @return mixed|string
     */
    public function getLogStore()
    {
        return $this->logger->getLogStore();
    }


    /**
     * @param $value
     *
     * @return $this
     */
    public function setLogStore($value)
    {
        $this->logger->setLogStore($value);

        return $this;
    }

    public function setQueue($queue)
    {
        $this->logger->setQueue($queue);
        return $this;
    }
}
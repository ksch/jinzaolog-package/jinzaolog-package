<?php

namespace Jinzaolog\Log;

require_once realpath(dirname(__FILE__) . '/../../aliyun-log-php-sdk/Log_Autoload.php');

class SLSLog
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $project;

    /**
     * @var string
     */
    protected $logStore;


    protected $queue = true;


    public function __construct(LogReq $client)
    {
        $this->client = $client;
    }


    /**
     * List log stores in project
     *
     * @param string $project
     *
     * @return ListLogStoresResponse
     */
    public function listLogStores($project = null)
    {
        $response = $this->client->listLogStores($project);
        return $response;
    }


    /**
     * Write logs to store
     *
     * @param array  $data
     *
     * @param string $topic
     *
     * @return bool
     */
    public function putLogs($data, $topic = null, $source = null, $time = null, $shardKey = null)
    {
        $time || $time = isset($data['time']) && $data['time'] ? strtotime($data['time']) : null;
        $logItem  = $this->client->createLogItem($data, $time);
        $response = $this->client->putLogs($this->project, $this->logStore,
            [ $logItem ], $topic, $source, $shardKey, $this->queue);

        return $response;
    }


    /**
     * List topics in store
     *
     * @return ListTopicsResponse
     */
    public function listTopics()
    {
        $response = $this->client->listTopics($this->project, $this->logStore);
        return $response;
    }


    /**
     * Get history logs
     *
     * @param integer $from
     * @param integer $to
     * @param string  $query
     * @param string  $topic
     *
     * @return GetHistogramsResponse
     */
    public function getHistograms($from = null, $to = null, $query = null, $topic = null)
    {
        $response = $this->client->getHistograms($this->project, $this->logStore, $from, $to, $topic, $query);

        return $response;
    }


    /**
     * Get logs in store
     *
     * @param string  $from
     * @param string  $to
     * @param string  $query
     * @param string  $topic
     * @param int     $line
     * @param string  $offset
     * @param boolean $reverse
     *
     * @return GetLogsResponse
     */
    public function getLogs(
        $from = null,
        $to = null,
        $query = null,
        $topic = null,
        $line = 100,
        $offset = null,
        $reverse = true
    ) {
        $response = $this->client->getLogs($this->project, $this->logStore, $from, $to, $topic, $query, $line, $offset, $reverse);
        return $response;
    }


    /**
     * @return mixed|string
     */
    public function getProject()
    {
        return $this->project;
    }


    /**
     * @param $value
     *
     * @return $this
     */
    public function setProject($value)
    {
        $this->project = $value;

        return $this;
    }


    /**
     * @return mixed|string
     */
    public function getLogStore()
    {
        return $this->logStore;
    }


    /**
     * @param $value
     *
     * @return $this
     */
    public function setLogStore($value)
    {
        $this->logStore = $value;

        return $this;
    }


    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    public function setQueue($queue) {
        $this->queue = $queue;
        return $this;
    }

}
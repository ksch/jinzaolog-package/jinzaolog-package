<?php

namespace Jinzaolog\Log;

use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 2018-11-29
 * Time: 11:34
 */
//namespace Jinzao\PhpSdk;
//require_once realpath(dirname(__FILE__) . '/../aliyun-log-php/Log_Autoload.php');
require_once realpath(dirname(__FILE__) . '/../../aliyun-log-php-sdk/Log_Autoload.php');

class LogReq
{

    use DispatchesJobs;

    protected $client;
    protected $token;

    protected $data = null;

    public function __construct($token = '')
    {

        $endpoint = config('jinzaolog.endpoint');
        $accessKeyId = config('jinzaolog.access_key_id');
        $accessKeySecret = config('jinzaolog.access_key_secret');

        $this->client = new \Aliyun_Log_Client($endpoint, $accessKeyId, $accessKeySecret);
        $this->token = $token;
    }

    public function createLogItem($data, $time = null)
    {
        $data = array_map(function(& $item) {
            if(is_array($item)){
                return json_encode($item);
            } else if(is_null($item)){
                return '';
            }
            return $item;
        }, $data);
        return new \Aliyun_Log_Models_LogItem($time, $data);
    }

    public function putLogs($project, $logstore, $logitems, $topic = null, $source = null, $shardKey = null, $queue = true)
    {
        $this->data = compact('project', 'logstore', 'logitems', 'topic', 'source', 'shardKey');
        if($queue){
            $this->firePutLogEvent();
        } else {
            $this->sendLogs();
        }
    }

    public function sendLogs()
    {
        $request = new \Aliyun_Log_Models_PutLogsRequest($this->data['project'], $this->data['logstore'],
            $this->data['topic'], $this->data['source'], $this->data['logitems'], $this->data['shardKey']);
        try {
            return $this->client->putLogs($request);
        } catch (\Aliyun_Log_Exception $ex) {
            throw new \Aliyun_Log_Exception($ex->getErrorCode(), $ex->getMessage());
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * Fires a log event.
     *
     * @return void
     */
    protected function firePutLogEvent()
    {
        $logJob = config('jinzaolog.queueJob', 'Jinzaolog\Job\SendJinzaoLogJob');
        if (!class_exists($logJob)) {
            throw new \Exception("Class [$logJob] does not exists.");
        }
        $this->dispatch(new $logJob($this));
    }

    public function listLogstores($project)
    {
        try {
            $request = new \Aliyun_Log_Models_ListLogstoresRequest($project);
            return $this->client->listLogstores($request);
        } catch (\Aliyun_Log_Exception $ex) {
            throw new \Aliyun_Log_Exception($ex->getErrorCode(), $ex->getMessage());
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }


    public function listTopics($project, $logstore, $token = null, $line = null)
    {
        $request = new \Aliyun_Log_Models_ListTopicsRequest($project, $logstore, $token, $line);

        try {
            return $this->client->listTopics($request);
        } catch (\Aliyun_Log_Exception $ex) {
            throw new \Aliyun_Log_Exception($ex->getErrorCode(), $ex->getMessage());
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function getLogs($project, $logstore, $from, $to, $topic = null, $query = null, $line = 100, $offset = 0, $reverse = false)
    {
        $request = new \Aliyun_Log_Models_GetLogsRequest($project, $logstore, $from, $to, $topic, $query, $line, $offset, $reverse);
        try {
            return $this->client->getLogs($request);
        } catch (\Aliyun_Log_Exception $ex) {
            throw new \Aliyun_Log_Exception($ex->getErrorCode(), $ex->getMessage());
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function getHistograms($project, $logstore, $from, $to, $topic = null, $query = null)
    {
        $request = new \Aliyun_Log_Models_GetHistogramsRequest($project, $logstore, $from, $to, $topic, $query);

        try {
            return $this->client->getHistograms($request);
        } catch (\Aliyun_Log_Exception $ex) {
            throw new \Aliyun_Log_Exception($ex->getErrorCode(), $ex->getMessage());
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function listShard($project, $logstore)
    {
        $request = new \Aliyun_Log_Models_ListShardsRequest($project, $logstore);
        try {
            return $this->client->listShards($request);
        } catch (\Aliyun_Log_Exception $ex) {
            throw new \Aliyun_Log_Exception($ex->getErrorCode(), $ex->getMessage());
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function batchGetLogs($project, $logstore)
    {
        $listShardRequest = new \Aliyun_Log_Models_ListShardsRequest($project, $logstore);
        $listShardResponse = $this->client->listShards($listShardRequest);
        foreach ($listShardResponse->getShardIds() as $shardId) {
            $getCursorRequest = new \Aliyun_Log_Models_GetCursorRequest($project, $logstore, $shardId, null, time() - 60);
            return $this->client->getCursor($getCursorRequest);
            $cursor = $response->getCursor();
            $count = 100;
            while (true) {
                $batchGetDataRequest = new \Aliyun_Log_Models_BatchGetLogsRequest($project, $logstore, $shardId, $count, $cursor);
                return $this->client->batchGetLogs($batchGetDataRequest);
                if ($cursor == $response->getNextCursor()) {
                    break;
                }
                $logGroupList = $response->getLogGroupList();
                foreach ($logGroupList as $logGroup) {
                    print ($logGroup->getCategory());

                    foreach ($logGroup->getLogsArray() as $log) {
                        foreach ($log->getContentsArray() as $content) {
                            print($content->getKey() . ":" . $content->getValue() . "\t");
                        }
                        print("\n");
                    }
                }
                $cursor = $response->getNextCursor();
            }
        }
    }

    public function deleteShard($project, $logstore, $shardId)
    {
        $request = new \Aliyun_Log_Models_DeleteShardRequest($project, $logstore, $shardId);
        try {
            return $this->client->deleteShard($request);
        } catch (\Aliyun_Log_Exception $ex) {
            throw new \Aliyun_Log_Exception($ex->getErrorCode(), $ex->getMessage());
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function mergeShard($project, $logstore, $shardId)
    {
        $request = new \Aliyun_Log_Models_MergeShardsRequest($project, $logstore, $shardId);
        try {
            return $this->client->mergeShards($request);
        } catch (\Aliyun_Log_Exception $ex) {
            throw new \Aliyun_Log_Exception($ex->getErrorCode(), $ex->getMessage());
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function splitShard($project, $logstore, $shardId, $midHash)
    {
        $request = new \Aliyun_Log_Models_SplitShardRequest($project, $logstore, $shardId, $midHash);
        try {
            return $this->client->splitShard($request);

        } catch (\Aliyun_Log_Exception $ex) {
            throw new \Aliyun_Log_Exception($ex->getErrorCode(), $ex->getMessage());
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function createLogStore($project, $logstore, $ttl = null, $shardCount = null)
    {
        $listStore = self::listLogstores($project);
        if ($listStore->getCount() && in_array($logstore, $listStore->getLogstores())) {
            throw new \Exception("logstore {$logstore} already exists");
        }
        $req2 = new \Aliyun_Log_Models_CreateLogstoreRequest($project, $logstore, $ttl, $shardCount);
        return $this->client->createLogstore($req2);
    }
}



<?php
namespace Jinzao\AliyunLogs;

class LogServe extends \Aliyun_Log_Client
{
    public function __construct($token = null)
    {
        $endpoint = config('aliyun-logs.endpoint');
        $accessKeyId = config('aliyun-logs.access_key_id');
        $accessKey = config('aliyun-logs.access_key');

        parent::__construct($endpoint, $accessKeyId, $accessKey, $token);
    }
}
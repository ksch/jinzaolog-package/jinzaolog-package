<?php

namespace Jinzaolog\Job;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Jinzaolog\Log\LogReq;

class SendJinzaoLogJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $queue = 'log';

    public $logReq;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(LogReq $logReq)
    {
        $this->logReq = $logReq;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->logReq->sendLogs();
    }
}

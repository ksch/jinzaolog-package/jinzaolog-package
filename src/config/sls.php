<?php

return [
    'access_key_id'     => env('ALIYUN_ACCESS_KEY_ID'),
    'access_key_secret' => env('ALIYUN_ACCESS_KEY_SECRET'),
    'endpoint'          => env('SLS_ENDPOINT'),
    'project'           => env('SLS_PROJECT'),
    'store'             => env('SLS_STORE'),
    'topic'             => null,
];
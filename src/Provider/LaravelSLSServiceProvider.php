<?php

namespace Jinzaolog\Provider;

use Aliyun\SLS\Client;
use Illuminate\Support\ServiceProvider;
use Jinzaolog\Log\LogReq;
use Jinzaolog\Log\SLSLog;
use Jinzaolog\Log\Writer;

class LaravelSLSServiceProvider extends ServiceProvider
{

    protected $logReq;

    public function boot()
    {
        $this->publishes([
            realpath(__DIR__ . '/../config/sls.php') => config_path('jinzaolog.php'),
//            realpath(__DIR__ . '/../Job/LaravelSLSServiceProvider.php') => app_path('Jobs/SendJinzaoLogJob.php'),
        ]);
    }


    /**
     * Add the connector to the queue drivers.
     *
     * @return void
     */
    public function register()
    {
        $this->logReq = new LogReq();
        $this->app->singleton("alilog", function(){
            return $this->logReq;
        });

        $this->app->bind('sls', function () {
            return $this->createSls();
        });

        $this->app->instance('sls.writer', new Writer($this->createSls(), $this->app['events'], config('jinzaolog.topic')));
    }

    public function createSls()
    {
        $log = new SLSLog(app('alilog'));
        $log->setProject(config('jinzaolog.project'));
        $log->setLogStore(config('jinzaolog.store'));

        return $log;
    }
}
